---
title: "Fernando de Noronha"
date: 2021-09-18T23:27:57-03:00
draft: false
---
Descoberto em 1503 e formado por 21 ilhas, o arquipélago Fernando de Noronha pertence ao estado de Pernambuco e fica distante 545km de Recife. No dia 13 de dezembro de 2001, a UNESCO considerou o arquipélago Patrimônio Mundial Natural e hoje a administração do Parque Nacional está atualmente a cargo do ICMBio (Instituto Chico Mendes de Conservação da Biodiversidade).

No geral, a ilha principal é dividida em duas partes: a do Mar de Fora que é voltado para o Continente Africano e o Mar de Dentro que é voltado para o Brasil. Inclusive, você sabia que Noronha está mais perto da África do que de São Paulo? 🙂

A seguir, listei algumas praias divididas por mar de fora e mar de dentro para ficar mais claro pra vocês.

Mar de Dentro

- Baía de Santo Antônio, (onde fica localizado o porto)
- Praia da Biboca
- Praia do Cachorro, na Vila dos Remédios (no centro histórico da ilha)
- Praia do Meio
- Praia da Conceição ou de Italcable
- Praia do Boldró, na Vila Boldró
- Praia do Americano
- Praia do Bode
- Praia da Quixabinha
- Praia da Cacimba do Padre
- Baía dos Porcos
- Baía do Sancho (baía de águas transparentes, cercada por falésias cobertas de vegetação)
- Baía dos Golfinhos ou Enseada do Carreiro de Pedra
- Ponta da Sapata

Mar de Fora

- Praia do Leão
- Ponta das Caracas
- Baía Sueste
- Praia de Atalaia
- Enseada da Caeira
- Buraco da Raquel
- Ponta da Air France

Como você já deve ter ouvido falar, para curtir as maravilhas naturais de Fernando de Noronha, é preciso separar parte do seu orçamento para pagar as taxas ambientais de preservação dos recursos naturais do arquipélago. A Taxa de Preservação Ambiental (TPA) é uma arrecadação do Governo Estadual de Pernambuco, que administra Fernando de Noronha, para a manutenção dos serviços públicos do local e todos os visitantes devem pagar a TPA.

O valor total desta taxa irá depender da quantidade de dias que você irá permanecer em Fernando de Noronha. Por isso, multiplique o valor de 1 diária (R$87,71) pelos dias que pretende permanecer na ilha. Ela pode ser paga online, o que eu recomendo muito, pois você já chegará no destino com tudo certo, assim como eu fiz.

Assim que você chegar no aeroporto, antes mesmo de pegar as suas bagagens, terá uma fila para apresentar ou comprovantes de pagamento ou efetuar o pagamento na hora. Eu, como fiz pela internet, quase não peguei fila e foi tudo muito tranquilo.

Ao final do processo de check-in na ilha, você receberá uma via que deve ficar com a sua hospedagem e um para fazer o check-out para sair da ilha. Guarde esses documentos pois você irá precisar muito deles.