+++
title = 'Sobre'
date = 2024-09-16T14:42:22-03:00
draft = false
+++

Bem Vindo ao meu blog de viagens. Sou o Henrique Zenga Carrenho, estudante de engenharia Mecatrônica na Universaidade de São Paulo (USP) e aqui compartilho um pouco sobre as minhas viagens com outras pessoas, como dicas de aeroporto, como fazer uma mala, alimentação local e compras. Eu moro em Vinhedo-SP e costuma fazer viagens nas minhas férias da faculdade. Sou uma pessoa muito metódica e organizada, então espero que essas dicas ajude você. Se quiser tirar alguma dúvida comigo pode me mandar um email: riquecarrenho@gmail.com.